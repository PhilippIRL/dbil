window.stop();
document.documentElement.innerHTML = "";

if(window.location.href !== "https://www.bild.de/") {
    window.location.href = "https://www.bild.de/";
}

// remove all attributes from the html tag
Array.from(document.documentElement.attributes).forEach(attribute => {
    document.documentElement.removeAttribute(attribute.name);
});

var publicRoot = chrome.runtime.getURL("/public");

var titleTag = document.createElement("title");
titleTag.textContent = "dBil";
document.head.appendChild(titleTag);

var iframe = document.createElement("iframe");
iframe.src = publicRoot + "/dBil.html";
iframe.style = "width:100vw;height:100vh;position:absolute;top:0;left:0;border:none;";
document.body.appendChild(iframe);
